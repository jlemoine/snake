module.exports = {
  devServer: {
    disableHostCheck: true,
    sockHost: 'www.snake.test'
  },
  publicPath: process.env.NODE_ENV === 'production'
    ? '/snake/'
    : '/'
}
