import en from './en.json'
import fr from './fr.json'

import i18next from 'i18next'
import intervalPlural from 'i18next-intervalplural-postprocessor'
import VueI18Next from '@panter/vue-i18next'

i18next
  .use(intervalPlural)
  .init({
    lng: 'fr',
    resources: {
      en: {
        translation: en
      },
      fr: {
        translation: fr
      }
    }
  })
;

export default () => {
  return new VueI18Next(i18next)
}
