import { mapState } from "vuex";
import { DIRECTION_BOTTOM, DIRECTION_LEFT, DIRECTION_RIGHT, DIRECTION_TOP } from "../../../store/module/game";
import { slotSize } from "../../../model/screen";

export default {
  data() {
    return {
      direction: DIRECTION_TOP,
      transition: true,
    }
  },
  props: {
    segment: Object,
    previous: {
      required: true,
      validator: prop => typeof prop === 'object' || prop === undefined
    },
    margin: Number,
    border: Number,
  },
  computed: {
    ...mapState({
      grid_x: state => state.config.grid_x,
      grid_y: state => state.config.grid_y,
      nextDirection: state => state.game.nextDirection,
    }),
    slotSize() {
      return slotSize(this.grid_x, this.grid_y, this.margin, this.border)
    },
    noTransition() {
      // When on a border an looking in the inner side
      return !this.transition
        || this.segment.direction === DIRECTION_RIGHT && 1 === this.segment.x
        || this.segment.direction === DIRECTION_LEFT && this.segment.x === this.grid_x
        || this.segment.direction === DIRECTION_BOTTOM && 1 === this.segment.y
        || this.segment.direction === DIRECTION_TOP && this.segment.y === this.grid_y
      ;
    },
  },
  watch: {
    previous() {
      this.checkTransition();
    },
  },
  methods: {
    checkTransition() {
      this.transition = true;

      setTimeout(this.desaturateDirection, 190);

      this.direction = this.getDirection();
    },
    desaturateDirection() {
      this.transition = false;

      switch (this.direction) {
        case -1:
         this.direction = DIRECTION_LEFT;
         return true;
        case 4:
         this.direction = DIRECTION_TOP;
         return true;
      }

      return false;
    },
    getDirection() {
      var current = this.getFavoriteDirection();

      // If there was a previous direction, rotation might be in a different direction
      if (null !== this.previous) {
        if (this.previous.direction === DIRECTION_TOP && current === DIRECTION_LEFT) {
          return -1;
        }
        if (this.previous.direction === DIRECTION_LEFT && current === DIRECTION_TOP) {
          return 4;
        }
      }

      return current;
    },
    slotOffset(position) {
      // The slot part
      return (this.slotSize + this.margin * 2) * (position - 1)
        // The external part
        + this.margin
      ;
    },
    getFavoriteDirection() {
      return this.segment.direction;
    },
  },
  mounted() {
    this.checkTransition();
  },
}
