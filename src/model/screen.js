export const availableHeight = () => {
  return screen.availHeight - (window.outerHeight - window.innerHeight) - 180
};

export const availableWidth = () => {
  return screen.availWidth - (window.outerWidth - window.innerWidth) - 30
};

export const slotSize = (grid_x, grid_y, margin = 1, border = 1) => {
  return Math.floor(Math.min(
    (availableHeight() - grid_y * 2 + margin + border) / grid_y,
    (availableWidth() - grid_x * 2 + margin + border) / grid_x
  ))
};
