import About from "./components/About";
import Game from './components/Game';
import Home from './components/Home';
import Score from './components/Score';

export default [
    { path: '/', name: 'home', component: Home },
    { path: '/game', name: 'game', component: Game },
    { path: '/score', name: 'score', component: Score },
    { path: '/about', name: 'about', component: About },
]
