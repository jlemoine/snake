const getDefaultState = () => {
  return {
    name: '', // Player name
    init_length: 3,
    grid_x: 11, // Grid width
    grid_y: 15, // Grid height
    loop: true,
    bonus: true,
    locale: "fr",
    stepSpeed: 5, // Number of items before speed increase
    minSpeed: 4, // Ending speed
    maxSpeed: 20, // Can be slower than this
    speed: 20, // Starting speed, aka number of frames before a move
  };
}

const state = getDefaultState();

const mutations = {
  changeLocale(state, {locale}) {
    state.locale = locale;
  },
  reset(state) {
    Object.assign(state, getDefaultState())
  },
  update(state, {name, value}) {
    state[name] = value;
  },
};

const actions = {
  reset({commit}) {
    commit('reset');
  },
  changeLocale({commit}, locale) {
    commit('changeLocale', {
      locale,
    });
  },
  update({commit}, {name, value}) {
    commit('update', {
      name,
      value,
    });
  },
};

export default {
  namespaced: true,
  getters: {},
  state,
  mutations,
  actions,
}
