import { STATUS_WIN } from "./game";

const getDefaultState = () => {
  return {
    ladder: [],
  };
}

const state = getDefaultState();

const mutations = {
  reset(state) {
    Object.assign(state, getDefaultState())
  },
  save(state, result) {
    state.ladder.push(result);

    state.ladder.sort((a, b) => {
      if (a.points === b.points) {
        return 0;
      }

      return a.points > b.points ? 1 : -1;
    })

    // Keep only 10 best results
    if (state.ladder.length > 10) {
      state.ladder.pop();
    }
  }
};

const actions = {
  reset({dispatch, commit}) {
    commit('reset');
  },
  save({commit, rootState}) {
    commit('save', {
      score: rootState.game.points,
      victory: STATUS_WIN === rootState.game.status,
      name: rootState.config.name,
      date: new Date(),
    })
  }
};

export default {
  namespaced: true,
  getters: {},
  state,
  mutations,
  actions,
}
