export const STATUS_PENDING = 'STATUS_PENDING';
export const STATUS_RUNNING = 'STATUS_RUNNING';
export const STATUS_PAUSE = 'STATUS_PAUSE';
export const STATUS_LOST = 'STATUS_LOST';
export const STATUS_WIN = 'STATUS_WIN';

export const DIRECTION_TOP = 0;
export const DIRECTION_RIGHT = 1;
export const DIRECTION_BOTTOM = 2;
export const DIRECTION_LEFT = 3;

export const TURN_LEFT = -1;
export const TURN_RIGHT = 1;

export const ITEM_TYPE_FRUIT = 'ITEM_TYPE_FRUIT';
export const ITEM_TYPE_BIG_FRUIT = 'ITEM_TYPE_BIG_FRUIT';
export const ITEM_TYPE_BRIDGE = 'ITEM_TYPE_BRIDGE';

const getDefaultState = () => {
  return {
    status: STATUS_PENDING,
    // 0: top, 1: right, 2: bottom, 3: left
    nextDirection: DIRECTION_TOP, // If not null, change the direction on next frame
    body: [], // Array of segments {x: <int>, y: <int>, direction: <0-3>}
    prev: [], // Array of previous positions {x: <int>, y: <int>, direction: <0-3>}, used for animation direction
    items: [], // Array of item on the grid {x: <int>, y: <int>, type: ITEM_TYPE_*, timeout: <int>}
    stomach: 0, // On each frame, if stomach > 0, add a segment to body
    bonuses: {
      eggs: 0,
      bridge: 0,
    },
    // +10 / fruit
    // +5 if bonus
    points: 0,
    // Interval
    moving: false,
    nextItem: null,
    frame: 0,
    speed: 20, // Starting speed, aka number of frames before a move
    nextSpeed: 0, // Once nextSpeed reach stepSpeed, decrease speed
    stepSpeed: 5, // After 5 items speed accelerate by 1
    minSpeed: 4, // Ending speed
    maxSpeed: 20, // Can be slower than this
    interval: null,
  };
}

const state = getDefaultState();

const interval = ({state, dispatch}) => {
  dispatch('game/incrementFrame', null, {root: true})
    .then(() => {
      if (state.moving) {
        return dispatch('game/checkMove', null, {root: true})
      }

      // Else keep chain alive
      return new Promise((resolve) => resolve());
    })
    .then(() => {
      if (null !== state.nextItem) {
        return dispatch('game/eat', null, {root: true})
      }

      // Else keep chain alive
      return new Promise((resolve) => resolve());
    })
    .then(() => {
      if (!state.items.length) {
        return dispatch('game/spawnItem', null, {root: true})
      }

      // Else keep chain alive
      return new Promise((resolve) => resolve());
    })
    .then(() => {
      return dispatch('game/checkSpeed', null, {root: true})
    })
    .then(() => {
      if (STATUS_LOST === state.status) {
        return dispatch('game/stop', null, {root: true})
      }

      if (state.moving) {
        return dispatch('game/move', null, {root: true})
      }
    })
  ;
}

const getters = {
  isPending(state) {
    return STATUS_PENDING === state.status;
  },
  isRunning(state) {
    return STATUS_RUNNING === state.status;
  },
  isPaused(state) {
    return STATUS_PAUSE === state.status;
  },
  isDone(state) {
    return -1 !== [STATUS_LOST, STATUS_WIN].indexOf(state.status);
  },
  nextHead: (state) => ({direction, grid_x, grid_y, loop}) => {
    var currentHead = state.body[0],
      nextHead = {
        x: currentHead.x,
        y: currentHead.y,
        direction: direction,
      }
    ;

    switch (direction) {
      case 0:
        nextHead.y--;
        break;
      case 1:
        nextHead.x++;
        break;
      case 2:
        nextHead.y++;
        break;
      case 3:
        nextHead.x--;
        break;
    }

    if (loop) {
      if (nextHead.x < 1) {
        nextHead.x = grid_x;
      }
      if (nextHead.x > grid_x) {
        nextHead.x = 1;
      }
      if (nextHead.y < 1) {
        nextHead.y = grid_y;
      }
      if (nextHead.y > grid_y) {
        nextHead.y = 1;
      }
    }

    return nextHead;
  },
  segmentAt: (state) => ({x, y}) => {
    var a = x, b = y;

    return state.body.find(({x, y}) => a === x && b === y);
  },
};

const mutations = {
  reset(state, {
    init_length,
    grid_x,
    grid_y,
  }) {
    // Reset map
    Object.assign(state, getDefaultState());

    // Init snake body, it's defined on grid's center
    const y_middle = Math.ceil((grid_y - init_length) / 2);
    for (var i = 0; i < init_length; i++) {
      state.body.push({
        x: Math.ceil(grid_x / 2),
        y: i + y_middle,
        direction: DIRECTION_TOP
      })
    }

    state.prev = [...state.body];
  },
  spawnItem(state, {grid_x, grid_y, bonus}) {
    var cells = [],
      nbItems = Math.floor(Math.random() * 1.08) + 1
    ;

    // Create cells
    for (var i = 1; i <= grid_x; i++) {
      for (var j = 1; j <= grid_y; j++) {
        if (undefined === state.body.find(({x, y}) => i === x && j === y)) {
          cells.push({x: i, y: j});
        }
      }
    }

    // If there is no more cells, it's a victory
    if (0 === cells.length) {
      state.status = STATUS_WIN;
      state.points += 200;

      return;
    }

    if (nbItems > cells.length) {
      nbItems = cells.length;
    }

    for (i = 0; i < nbItems; i++) {
      // Fetch random cell
      var index = Math.floor(Math.random() * cells.length),
        type = ITEM_TYPE_FRUIT
      ;

      // Use another fruit type
      if (i > 0 && bonus) {
        type = ITEM_TYPE_BIG_FRUIT;
      }

      // Item is at cell[index]
      state.items.push({
        x: cells[index].x,
        y: cells[index].y,
        type,
        timeout: -1,
      })
    }
  },
  incrementFrame(state) {
    state.frame++;
    if (state.frame >= state.speed) {
      state.frame = 0;
      state.moving = true;
    }
  },
  start(state, {dispatch}) {
    state.status = STATUS_RUNNING;
    state.interval = setInterval(() => interval({state, dispatch}), 50);
  },
  stop(state) {
    state.status = STATUS_LOST;
    clearInterval(state.interval);
  },
  togglePause(state, {dispatch}) {
    if (STATUS_RUNNING !== state.status) {
      return dispatch('game/start', null, {root: true});
    }

    state.status = STATUS_PAUSE;
    clearInterval(state.interval);
  },
  nextDirection(state, {direction}) {
    var currentHead = state.body[0];
    // Dont return at 180°
    if (currentHead.direction + 2 === direction
      || currentHead.direction - 2 === direction
    ) {
      return;
    }

    state.nextDirection = direction;
  },
  turn(state, {way}) {
    var currentHead = state.body[0],
      direction = currentHead.direction
    ;

    direction += way;
    if (direction < DIRECTION_TOP) {
      direction = DIRECTION_LEFT;
    }
    if (direction > DIRECTION_LEFT) {
      direction = DIRECTION_TOP;
    }

    state.nextDirection = direction;
  },
  checkMove(state, {nextHead, segmentAt, grid_x, grid_y}) {
    // Check edges
    if (nextHead.x < 1 || nextHead.x > grid_x
      || nextHead.y < 1 || nextHead.y > grid_y
      || undefined !== segmentAt
    ) {
      state.status = STATUS_LOST;
    }


    var a = nextHead.x, b = nextHead.y;

    // Set item as next to be eaten
    var item = state.items.find(({x, y}) => a === x && b === y);
    if (undefined !== item) {
      state.nextItem = item;
    }
  },
  move(state, {nextHead}) {
    // Set prev as current body
    state.prev = [...state.body];

    // Add head
    state.body.unshift(nextHead);
    // Increase size if just eat something
    if (state.stomach > 0) {
      state.stomach--;
    } else {
      state.body.pop();
    }

    state.moving = false;
  },
  eat(state) {
    if (!state.nextItem) {
      return;
    }

    // Resolve item effets
    switch (state.nextItem.type) {
      case ITEM_TYPE_FRUIT:
        state.stomach++;
        break;
      case ITEM_TYPE_BIG_FRUIT:
        state.stomach += 2;
        state.points += 15;
        break;
    }

    state.points += 10;
    state.nextSpeed++;

    // Remove all items
    state.items = [];
    state.nextItem = null;
  },
  checkSpeed(state, {minSpeed, stepSpeed}) {
    if (state.nextSpeed >= stepSpeed) {
      state.nextSpeed = 0;
      // If we didn't reached minSpeed, increase speed (decrease number of frames)
      if (state.speed > minSpeed) {
        state.speed--;
      }
    }
  }
};

const actions = {
  reset({commit, rootState}) {
    commit('reset', {
      init_length: rootState.config.init_length,
      grid_x: rootState.config.grid_x,
      grid_y: rootState.config.grid_y,
    });
  },
  start({commit, dispatch}) {
    commit('start', {
      dispatch,
    });
  },
  incrementFrame({commit}) {
    commit('incrementFrame');
  },
  togglePause({commit, dispatch}) {
    commit('togglePause', {dispatch});
  },
  stop({commit}) {
    commit('stop');
  },
  nextDirection({commit}, {direction}) {
    commit('nextDirection', {direction});
  },
  checkMove({commit, state, rootState, getters}) {
    var nextHead = getters.nextHead({
      direction: state.nextDirection,
      grid_x: rootState.config.grid_x,
      grid_y: rootState.config.grid_y,
      loop: rootState.config.loop,
    });

    commit('checkMove', {
      nextHead,
      segmentAt: getters.segmentAt(nextHead),
      grid_x: rootState.config.grid_x,
      grid_y: rootState.config.grid_y,
    });
  },
  move({commit, state, getters, rootState}) {
    commit('move', {
      nextHead: getters.nextHead({
        direction: state.nextDirection,
        grid_x: rootState.config.grid_x,
        grid_y: rootState.config.grid_y,
        loop: rootState.config.loop,
      })
    });
  },
  eat({commit}) {
    commit('eat')
  },
  spawnItem({commit, rootState}) {
    commit('spawnItem', {
      grid_x: rootState.config.grid_x,
      grid_y: rootState.config.grid_y,
      bonus: rootState.config.bonus,
    })
  },
  checkSpeed({commit, rootState}) {
    commit('checkSpeed', {
      minSpeed: rootState.config.minSpeed,
      maxSpeed: rootState.config.maxSpeed,
      stepSpeed: rootState.config.stepSpeed,
    });
  },
  turnLeft({commit}) {
    commit('turn', {way: TURN_LEFT});
  },
  turnRight({commit}) {
    commit('turn', {way: TURN_RIGHT});
  },
};

export default {
  namespaced: true,
  getters,
  state,
  mutations,
  actions,
}
