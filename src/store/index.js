import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'
import config from "./module/config"
import game from "./module/game"
import score from "./module/score"

Vue.use(Vuex)
const vuexPersist = new VuexPersist({
  key: 'snake',
  storage: window.localStorage
})

export const store = new Vuex.Store({
  namespaced: true,
  state: {},
  modules: {
    game,
    config,
    score,
  },
  actions: {
    reset() {
      window.localStorage.removeItem('snake');

      window.location.href = process.env.NODE_ENV === 'production'
        ? '/snake/'
        : '/';
    }
  },
  plugins: [vuexPersist.plugin],
  strict: process.env.NODE_ENV !== 'production',
})
