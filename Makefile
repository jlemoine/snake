DOCKER_TOOLBOX_CONTAINER_NAME = snake_node

STEP = "\\n\\r**************************************************\\n"

help:
	@echo "-- build: builds the docker images required to run this project"; \
	echo "-- build-force: Force rebuilds the docker images required to run this project"; \
	echo "-- start: starts the project"; \
	echo "-- stop: stops the project"; \
	echo "-- setup: setups the project (to run the first time you wan to launch the project)"; \
	echo "-- down: stops and removes containers used for this project (can generate data loss)"; \
	echo "-- cli: enters PHP container CLI"; \
	echo "-- publish: Update all routes and publish all pages on all sites"; \
	echo "-- logs: displays real time logs of containers used for this project"; \
	echo "-- console {command}: runs {command} in bin/console"; \
	echo "-- purge: Delete all container and images; \
	echo "-- watch: encore watch"; \
	echo "-- dist: Compress and minify assets, clean cache and create a tarball in ./dist"; \

build: do-init do-build do-finish
do-build:
	echo "$(STEP) Building images... $(STEP)"; \
	docker-compose -f docker-compose.yml build;

build-force: do-init do-build-force do-finish
do-build-force:
	@echo "$(STEP) Building images... $(STEP)"
	docker-compose -f docker-compose.yml build --no-cache

start: do-init do-start do-finish
do-start:
	echo "$(STEP) Starting up containers... $(STEP)"; \
	docker-compose -f docker-compose.yml up -d --remove-orphans; \

stop: do-init do-stop do-finish
do-stop:
	echo "$(STEP) Stopping containers... $(STEP)"; \
	docker-compose -f docker-compose.yml stop;

restart: do-init do-stop do-start do-finish

down: do-init do-down do-finish
do-down:
	echo "$(STEP) Stopping and removing containers... $(STEP)"; \
	docker-compose -f docker-compose.yml down;

ssh: do-init do-cli do-finish
do-ssh:
	echo "$(STEP) Cli Bash $(DOCKER_TOOLBOX_CONTAINER_NAME)... $(STEP)"; \
	docker exec -ti $(DOCKER_TOOLBOX_CONTAINER_NAME) /bin/bash;

watch: do-init do-watch do-finish
do-watch:
	echo "$(STEP) Running encore watch... $(STEP)"; \
	docker exec -u docker $(DOCKER_TOOLBOX_CONTAINER_NAME) yarn start;

dist: do-init do-dist do-finish
do-dist:
	docker exec -u docker $(DOCKER_TOOLBOX_CONTAINER_NAME) yarn run encore production;
	docker exec -u docker $(DOCKER_TOOLBOX_CONTAINER_NAME) ./docker/build.sh

sonata: do-init do-sonata do-finish
do-sonata:
	echo "$(STEP) Publish sonata pages... $(STEP)"; \
	docker exec -u docker $(DOCKER_TOOLBOX_CONTAINER_NAME) php bin/console sonata:page:update-core-routes --site=all;
	docker exec -u docker $(DOCKER_TOOLBOX_CONTAINER_NAME) php bin/console sonata:page:create-snapshots --site=all;

display-routes: do-init do-display-routes
do-display-routes:
	docker exec -u docker $(DOCKER_TOOLBOX_CONTAINER_NAME) php bin/console debug:router;

display-services: do-init do-display-services
do-display-services:
	docker exec -u docker $(DOCKER_TOOLBOX_CONTAINER_NAME) php bin/console debug:container;

logs: do-init do-logs do-finish
do-logs:
	echo "$(STEP) Displaying logs... $(STEP)"; \
	docker-compose -f docker-compose.yml logs -f --tail="100";

purge: do-init do-purge do-finish
do-purge:
	echo "Kill all containers"; \
	docker kill $(docker ps -q)
	echo "Delete all containers"; \
	docker rm $(docker ps -a -q)
	echo "Delete all images"; \
	docker rmi $(docker images -q)

do-init:
	@rm -f .docker-config; \
	touch .docker-config;
ifeq ($(OS),Darwin)
	@echo $$(docker-machine env default) >> .docker-config;
endif

do-finish:
	@rm -f .docker-config;
